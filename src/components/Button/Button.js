import React, {Component} from 'react';
import classes from './Button.module.scss';
import PropTypes from 'prop-types';


class Button extends Component {

    render() {
        const cls = [classes.Button];
        const {backgroundColor, onClickBtn, cardID} = this.props;

        return (
            <button
                className={cls.join(' ')}
                style={{backgroundColor}}
                onClick={() => onClickBtn(cardID)}
            >
                {this.props.children}
            </button>
        );
    }
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    onClickBtn: PropTypes.func,
    cardID: PropTypes.number
}

Button.defaultProps = {
    backgroundColor: 'orange',
    cardID: null,
}


export default Button;