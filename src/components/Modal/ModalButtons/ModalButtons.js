import React, {Component} from 'react';
import Button from "../../Button/Button";
import PropTypes from 'prop-types';

class ModalButtons extends Component {

    render() {

        const {toogleModal, toogleInCart, activeModalCard} = this.props

        return (
            <div>
                <Button onClickBtn={() => {
                    toogleInCart(activeModalCard)
                    toogleModal()
                }}>Ok</Button>
                <Button onClickBtn={() => toogleModal()}>Cancel</Button>
            </div>
        );
    }
}

ModalButtons.propTypes = {
    toogleModal: PropTypes.func,
    toogleInCart: PropTypes.func,
    activeModalCard: PropTypes.number
}


export default ModalButtons;