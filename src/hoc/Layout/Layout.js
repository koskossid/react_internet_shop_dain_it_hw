import React, {Component} from 'react';
import classes from './Layout.module.scss'
import Header from "../../container/Header/Header";
import Body from "../../container/Body/Body";
import Footer from "../../container/Footer/Footer";
import PropTypes from 'prop-types';

class Layout extends Component {
    render() {
        const {booksList, toogleInFavorite, modalIsOpen, toogleModal, toogleInCart, activeModalCard} = this.props;

        return (
            <div className={classes.Layout}>
                {/*<Header />*/}
                <Body
                    booksList={booksList}
                    toogleInFavorite={toogleInFavorite}
                    modalIsOpen={modalIsOpen}
                    toogleModal={toogleModal}
                    toogleInCart={toogleInCart}
                    activeModalCard={activeModalCard}
                />
                {/*<Footer />*/}
            </div>
        );
    }
}

Layout.propTypes = {
    booksList: PropTypes.array,
    toogleInFavorite: PropTypes.func,
    modalIsOpen: PropTypes.bool,
    toogleModal: PropTypes.func,
    toogleInCart: PropTypes.func,
    activeModalCard: PropTypes.number
}

export default Layout;