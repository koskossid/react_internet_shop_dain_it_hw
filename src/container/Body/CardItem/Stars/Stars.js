import React, {Component} from 'react';
import classes from './Stars.module.scss';
import PropTypes from 'prop-types';


class Stars extends Component {

    render() {
        const {id, inFavotite, toogleInFavorite} = this.props
        const cls = [classes.Stars, 'fas fa-star'];
        let starColor
        if(inFavotite) {
            starColor = 'orange'
        } else {
            starColor = 'black'
        }

        return (
            <i onClick={() => toogleInFavorite(id)} className={cls.join(' ')} style={{color: starColor}}></i>
        )
    }
}

Stars.propTypes = {
    toogleInFavorite: PropTypes.func,
    inFavotite: PropTypes.bool,
    id: PropTypes.number
}

export default Stars;