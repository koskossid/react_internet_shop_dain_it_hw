import React from 'react';
import classes from './App.module.scss';
import Layout from "./hoc/Layout/Layout";

class App extends React.Component {

    state = {
        booksList: null,
        modalIsOpen: false,
        activeModalCard: null
    }

    componentDidMount() {
        // setTimeout is for imitation of data receiving from server with delay
        setTimeout(() => {
            return fetch('./jsonData.json')
                .then(result => result.json())
                .then((list) => {
                    let booksList = list;
                    const inFavoriteID = JSON.parse(localStorage.getItem("inFavoriteStorage"));
                    const inCartID = JSON.parse(localStorage.getItem("inCartStorage"));
                    booksList.forEach((record) => {
                        if(inFavoriteID && inFavoriteID.includes(record.id)) record.inFavorite = true;
                        if(inCartID && inCartID.includes(record.id)) record.inCart = true
                    })
                    return booksList
                })
            .then(list => this.setState({booksList: list}))
        }, 1500)
    }

    toogleInFavorite = (id) => {
        let newBooksList = this.state.booksList
        newBooksList.forEach(book => {
            if(book.id === id) book.inFavorite = !book.inFavorite
        })
        this.setState({booksList: newBooksList})
        let idInFavorite = newBooksList.filter(book => book.inFavorite).map(book => book.id)
        localStorage.setItem('inFavoriteStorage', JSON.stringify(idInFavorite))
    }

    toogleInCart = (id) => {
        let newBooksList = this.state.booksList
        newBooksList.forEach(book => {
            if(book.id === id) book.inCart = !book.inCart
        })
        this.setState({booksList: newBooksList})
        let idInCart = newBooksList.filter(book => book.inCart).map(book => book.id)
        localStorage.setItem('inCartStorage', JSON.stringify(idInCart))
    }

    toogleModal = (id) => {
        this.setState(prevState => (
                {modalIsOpen: !prevState.modalIsOpen}
            )
        )
        if(id) this.setState({activeModalCard: id})
    }

    render() {
        const {booksList, modalIsOpen, activeModalCard} = this.state;
        return (
                <Layout
                    className={classes.AppBooksStore}
                    booksList={booksList}
                    modalIsOpen={modalIsOpen}
                    toogleModal={this.toogleModal}
                    toogleInFavorite={this.toogleInFavorite}
                    toogleInCart={this.toogleInCart}
                    activeModalCard={activeModalCard}
                />
        )
    }
}

export default App;
