import React, {Component} from 'react';
import classes from './Body.module.scss';
import Loader from "./Loader/Loader";
import CardItem from "./CardItem/CardItem";
import Modal from "../../components/Modal/Modal";
import {modalConstInput} from '../../components/Modal/ModalConstInput'
import PropTypes from 'prop-types';


class Body extends Component {

    render() {
        const {booksList, toogleInFavorite, modalIsOpen, toogleModal, toogleInCart, activeModalCard} = this.props;
        const {addToCart, removeFromCart} = modalConstInput;

        if (!booksList) {
            return <Loader/>
        }
        let modalHeader, modalText;
        if(activeModalCard) {
            const modalType = booksList.filter(book => book.id === activeModalCard)

            if(modalType[0].inCart) {
                modalHeader = removeFromCart.modalHeader
                modalText = removeFromCart.modalText
            } else {
                modalHeader = addToCart.modalHeader
                modalText = addToCart.modalText
            }
        }

        return (
            <div className={classes.Body}>
                <main className={classes.booksContainer}>
                    {booksList.map((book) => {
                        return <CardItem
                            key={book.id}
                            book={book}
                            toogleInFavorite={toogleInFavorite}
                            toogleModal={toogleModal}
                        />
                    })}
                </main>
                {modalIsOpen ?
                    <Modal
                        modalHeader={modalHeader}
                        modalText={modalText}
                        toogleModal={toogleModal}
                        toogleInCart={toogleInCart}
                        activeModalCard={activeModalCard}
                        booksList={booksList}
                    />
                    : null
                }
            </div>
        )
    }
}

Body.propTypes = {
    booksList: PropTypes.array,
    toogleInFavorite: PropTypes.func,
    modalIsOpen: PropTypes.bool,
    toogleModal: PropTypes.func,
    toogleInCart: PropTypes.func,
    activeModalCard: PropTypes.number
}

export default Body;