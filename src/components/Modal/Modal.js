import React, {Component} from 'react';
import classes from './Modal.module.scss';
import BackDrop from "./BackDrop/BackDrop";
import CloseModalButton from "./CloseModalButton/CloseModalButton";
import PropTypes from 'prop-types';
import ModalButtons from "./ModalButtons/ModalButtons";

class Modal extends Component {

    render() {

        const {toogleModal, toogleInCart, activeModalCard, modalHeader, modalText} = this.props;
        return (
            <React.Fragment>
                <div className={classes.Modal}>
                    <div className={classes.Modal__title}>
                        <h2><strong>{modalHeader}</strong></h2>
                            <CloseModalButton
                                toogleModal={toogleModal}
                            />
                    </div>
                    <div className={classes.Modal__text}>
                        <p>{modalText}</p>
                        <ModalButtons toogleModal={toogleModal} toogleInCart={toogleInCart} activeModalCard={activeModalCard} />
                    </div>
                </div>
                <BackDrop toogleModal={toogleModal} />
            </React.Fragment>
        );
    }
}

Modal.propTypes = {
    toogleModal: PropTypes.func,
    toogleInCart: PropTypes.func,
    activeModalCard: PropTypes.number,
    modalHeader: PropTypes.string,
    modalText: PropTypes.string
}

export default Modal;