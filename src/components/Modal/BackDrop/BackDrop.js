import React, {Component} from 'react';
import classes from './BackDrop.module.scss';
import PropTypes from 'prop-types';

class BackDrop extends Component {

    render() {
        const {toogleModal} = this.props
        return (
            <div
                className={classes.BackDrop}
                onClick={() => toogleModal()}
            />
        );
    }
}

BackDrop.propTypes = {
    toogleModal: PropTypes.func
}

export default BackDrop;