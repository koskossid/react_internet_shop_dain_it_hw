import React, {Component} from 'react';
import classes from './CardItem.module.scss'
import Button from "../../../components/Button/Button";
import Stars from "./Stars/Stars";
import PropTypes from 'prop-types';


class CardItem extends Component {

    render() {
        let {book: {name, price, imgUrl, id, color, inCart, inFavorite}, toogleInFavorite, toogleModal} = this.props;
        let cardText;
        if(inCart) {
            cardText = 'Book in Cart'
            color = 'green'
        } else {
            cardText = 'Add to Cart'
        }

        return (
            <div className={classes.CardItem}>
                <div className={classes.cardImg}>
                    <img src={imgUrl} alt="card_image"></img>
                </div>
                <div className={classes.cardDescription}>
                    <p>id({id}): {name} {<Stars id={id} inFavotite={inFavorite} toogleInFavorite={toogleInFavorite}/>}</p>
                    <div className={classes.cardPrice}>
                        <p><strong>Price {price}$</strong></p>
                        <Button backgroundColor={color} cardID={id} onClickBtn={toogleModal} >{cardText}</Button>
                    </div>

                </div>
            </div>
        );
    }
}

CardItem.propTypes = {
    toogleInFavorite: PropTypes.func,
    toogleModal: PropTypes.func,
    book: PropTypes.exact({
        name: PropTypes.string,
        price: PropTypes.number,
        imgUrl: PropTypes.string,
        id: PropTypes.number,
        color: PropTypes.string,
        inCart: PropTypes.bool,
        inFavorite: PropTypes.bool
    })
}

export default CardItem;