import React, {Component} from 'react';
import classes from './Loader.module.scss'

class Loader extends Component {
    render() {
        return (
            <div className={classes.Loader}>
                <h1>Request data from server ...</h1>
            </div>
        );
    }
}

export default Loader;