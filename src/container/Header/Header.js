import React, {Component} from 'react';
import classes from './Header.module.scss';
class Header extends Component {

    render() {

        return (
            <div className={classes.Header}>
                <h1>BOOKS SHOP</h1>
            </div>
        );
    }
}

export default Header;