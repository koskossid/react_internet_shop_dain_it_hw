import React, {Component} from 'react';
import classes from './CloseModalButton.module.scss';
import PropTypes from 'prop-types';


class CloseModalButton extends Component {
    render() {
        const {toogleModal} = this.props
        const cls = [classes.CloseModalButton, 'fa fa-times']
        return (
            <i className={cls.join(' ')} onClick={() => toogleModal()}/>
        );
    }
}

CloseModalButton.propTypes = {
    toogleModal: PropTypes.func,
}

export default CloseModalButton;